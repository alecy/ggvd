#!/usr/bin/python
# -*-coding:utf-8 -*

import sys
import re
import argparse
import os

parser = argparse.ArgumentParser()
parser.add_argument("--size_gram", help="n-gram")
args = parser.parse_args()
n = int(args.size_gram)

for line in sys.stdin:
    file_name = re.sub(r' ', r'', str(os.getenv("map_input_file", "not_find.txt")))
    line_clean = re.sub(r'[^\w\s]', r'', str(line))

    n_grams = []
    for i in range(0, len(line_clean.split()) - n-1):
        gram = [" ".join(line_clean.split()[i:i+n])]
        n_grams.append(gram)

    for item in n_grams:
        print(" ".join(item), 1, file_name)