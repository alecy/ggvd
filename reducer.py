#!/usr/bin/python
# -*-coding:utf-8 -*

import sys
import argparse
parser = argparse.ArgumentParser()
parser.add_argument("--minimum_frequency")
args = parser.parse_args()
min_freq = int(args.minimum_frequency)

line_old = []
for line in sys.stdin:
    list_of_words = line.split(" ")
    n_gram = " ".join(list_of_words[0: len(list_of_words)-2])
    file_name = list_of_words[len(list_of_words)-1].split("/")[len(list_of_words[len(list_of_words)-1].split("/"))-1]
    count = int(list_of_words[len(list_of_words)-2])
    if line_old:
        if n_gram == line_old[0]:
            if file_name not in line_old[2]:
                file_name = " ".join([line_old[2], file_name])
            count = line_old[1]+1

        elif int(line_old[1]) >= min_freq:
            print(*line_old)

    line_old = [n_gram, count, file_name]