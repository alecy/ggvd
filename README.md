# How to execute the code in Hadoop
- Clone the docker: https://gitlab.com/alecy/docker_hadoop_spark
- Start the docker
```bash
$ cd FILE_DOCKER
$ docker-compose up
```
- Copy all the files from directory to namenode
- Inside namenode copy files from directory to HDFS:
```bash
$ hdfs dfs -mkdir -p input
$ hdfs dfs -put ./input/* input
```
- Find the library Jarfile from Hadoop:
```bash
$ find / -name 'hadoop-streaming*.jar'
```
- Execute the program with the parameters size_gram (size of the gram), minimum_frequency (the minimum frequency of the gram) and the path /share/hadoop/tools/lib/hadoop-streaming-3.2.1.jar is the path fing in the step before after the /opt/hadoop-3.2.1:
```bash
$ hadoop jar $HADOOP_HOME/share/hadoop/tools/lib/hadoop-streaming-3.2.1.jar -file ./PATH/mapper.py -mapper "/usr/bin/python3 mapper.py --size_gram 3" -file ./PATH/reducer.py -reducer "/usr/bin/python3 reducer.py --minimum_frequency 4" -input PATH_hdfs/input -output PATH_hdfs/output
```